﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceProcedure
{
    internal class Rest
    {
        private static readonly HttpClient client = new HttpClient();

        public static bool GetData(string url, string auth)
        {
            bool result = false;

            try
            {
                if (!string.IsNullOrEmpty(auth))
                {
                    var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.UTF8.GetBytes(auth));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
                }

                var response = client.GetAsync(url).Result;

                if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                {
                    result = true;
                }
                else
                {
                    result = false;
                    string resp = response.Content.ReadAsStringAsync().Result;
                    Program.WriteToLog(Environment.NewLine + "Felmeddelande från Ariba: " + resp + Environment.NewLine);
                    //resp = resp.Replace("\"", "");
                }
            }
            catch (Exception e)
            {
                Program.WriteToLog(Environment.NewLine + "Fel i anrop till Ariba" + Environment.NewLine + e + Environment.NewLine);
            }

            return result;
        }

        public static string PostData(string url, object body, string auth)
        {
            string result = "";

            try
            {
                /*  Out-commented for now, becase then we can skip adding Newtonsoft
                if (!string.IsNullOrEmpty(auth))
                {
                    var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.UTF8.GetBytes(auth));
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
                }

                var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");

                var response = client.PostAsync(url, content).Result;

                if ((int)response.StatusCode >= 200 && (int)response.StatusCode <= 299)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    result = "ERROR";
                }
                */
            }
            catch (Exception e)
            {
                
            }

            return result;
        }
    }
}
