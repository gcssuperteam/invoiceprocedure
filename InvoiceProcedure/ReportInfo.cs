﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceProcedure
{
    public class ReportInfo
    {
        public string Generator { get; set; }
        public string Report { get; set; }
        public string Medium { get; set; }
        public string Blankett { get; set; }
        public string Dialog1 { get; set; }
        public string Answer1 { get; set; }
    }
}
