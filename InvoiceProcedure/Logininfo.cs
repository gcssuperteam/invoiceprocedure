﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace InvoiceProcedure
{
    public class Logininfo
    {
        public string Bolag { get; set; }
        public string Usr { get; set; }
        public string Password { get; set; }
        public string GarpConfig { get; set; }
        public bool TraningMode { get; set; }
        public string SkrivLoggFil { get; set; }
        public string LogFileName { get; set; }
        public string ReportInfo { get; set; }
        public int MonthSaveLog { get; set; }
        public string FolderArchive { get; set; }
        public string AppArchive { get; set; }
        public string AppMail { get; set; }
        public string AppStartArgument { get; set; }
        public string KundKodEpost { get; set; }
        public string BlankettDirekt { get; set; }
        public string BlankettBuy { get; set; }
        public string Dok1tknDirekt { get; set; }
        public string Dok1tknBuy { get; set; }
        public string DokEDIKopia { get; set; }
        public string UrlAribaApi { get; set; }

        public Logininfo()
        {
            KundKodEpost = ConfigurationManager.AppSettings["KundKodEpost"];

            BlankettDirekt = ConfigurationManager.AppSettings["BlankettDirekt"];
            BlankettBuy = ConfigurationManager.AppSettings["BlankettEasyInvoice"];
            Dok1tknDirekt = ConfigurationManager.AppSettings["Dokument1tknDirekt"];
            Dok1tknBuy = ConfigurationManager.AppSettings["Dokument1tknKöp"];
            DokEDIKopia = ConfigurationManager.AppSettings["DokumentEDIKopia"];

            Bolag = ConfigurationManager.AppSettings["Bolag"];
            Usr = ConfigurationManager.AppSettings["Användare"];
            Password = ConfigurationManager.AppSettings["Lösen"];
            GarpConfig = ConfigurationManager.AppSettings["Konfiguration"];
            if (ConfigurationManager.AppSettings["Övningsbolag"] == "True")
            {
                TraningMode = true;
            }
            if (ConfigurationManager.AppSettings["SkrivLog"] == "True")
            {
                SkrivLoggFil = ConfigurationManager.AppSettings["SkrivLog"];
            }

            LogFileName = string.Format(@"{0}"
            , Path.GetDirectoryName(ConfigurationManager.AppSettings["FilFörLog"])
            + @"\"
            + Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["FilFörLog"])
            + " "
            + DateTime.Now.ToString("yyyy-MM-dd HHmm"))
            + Path.GetExtension(ConfigurationManager.AppSettings["FilFörLog"]);

            MonthSaveLog = int.Parse(ConfigurationManager.AppSettings["AntalMånaderSparaLog"]);
            ReportInfo = string.Format(@"{0}", ConfigurationManager.AppSettings["FilMedRapportInfo"]);

            FolderArchive = string.Format(@"{0}", ConfigurationManager.AppSettings["MappArkiveraFakturor"]);

            AppArchive = string.Format(@"{0}", ConfigurationManager.AppSettings["ProgramArkivera"]);
            AppMail = string.Format(@"{0}", ConfigurationManager.AppSettings["ProgramMaila"]);
            AppStartArgument = string.Format(@"{0}", ConfigurationManager.AppSettings["StartaMedArgument"]);

            UrlAribaApi = string.Format(@"{0}", ConfigurationManager.AppSettings["UrlAribaApi"]);
        }
    }
}
