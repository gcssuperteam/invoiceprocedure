﻿Program för fakturering med flera fakturadokument

Dokumenten anges i textlfilen som skall ligga i samma mapp som programmet
Även App.config måste finnas i samma mapp

Fakturerar allt ofakturerat max 1 år bakåt

Fakturorna skall ha läsning följesedel

Version 1.6
Lagt in anrop för utskick av Ericssonfakturor via Ariba/EDI

Version 1.5
Lagt in versionsnummer i loggen
Justerat villkor för utswkrift till papper

Version 1.4
Villkor tillagt för test om faktura skall skickas via EDI, både via fakturaköp och direktfaktura

Version 1.3
För varje följesedel testas om fakturan skall skrivas ut till skrivare eller inte

Version 1.2
Test görs vilka fakturadokument som kommer att köras

Version 1.1
Första test att starta arkivering och mail