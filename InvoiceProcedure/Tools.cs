﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceProcedure
{
    public class Tools
    {
        /// <summary>
        /// Antal tecken från vänster
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="length">Längd från vänster</param>
        /// <returns></returns>
        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        /// Antal tecken från höger
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="length">Längd från höger</param>
        /// <returns></returns>
        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// Tecken i mitten
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="startIndex">Startposition</param>
        /// <param name="length">Antal tecken</param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        /// Från mitten till höger
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="startIndex">Startposition</param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        /// <summary>
        /// Fyll ut blanktecken till höger
        /// </summary>
        /// <param name="param">text</param>
        /// <param name="length">antal tecken</param>
        /// <returns></returns>
        public static string fillBlankRight(string param, int length)
        {
            string result = param.PadRight(length, ' ');
            return result;
        }

        /// <summary>
        /// Fyll ut blanktecken till vänster
        /// </summary>
        /// <param name="param">text</param>
        /// <param name="length">antal tecken</param>
        /// <returns></returns>
        public static string fillBlankLeft(string param, int length)
        {
            string result = param.PadLeft(length, ' ');
            return result;
        }

        /// <summary>
        /// Kollar om stränger ligger i ett intervall mellan två andra strängar
        ///  Ger resultatet 1 eller 0
        /// </summary>
        /// <param name="MyString"></param>
        /// <param name="From"></param>
        /// <param name="To"></param>
        /// <returns></returns>
        public static int CompareBetween(string MyString, string From, string To)
        {
            int result = 0;
            if (MyString == From || MyString == To || (string.Compare(MyString, From) > 0 && string.Compare(MyString, To) < 0))
            {
                result = 1;
            }
            return result;
        }
    }
}
