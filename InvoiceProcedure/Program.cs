﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace InvoiceProcedure
{
    public class Program
    {
        static Garp.Application GarpApp;
        static Logininfo login;
        static List<ReportInfo> ReportToRun;


        static void Main(string[] args)
        {
            try
            {
                login = new Logininfo();
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar, version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + Environment.NewLine);
                bool OK = ReadyToStart();
                if (OK)
                {
                    GarpLogin();
                    ReadSettingsFile();
                    RunReportAndMail();
                    SendAribaInvoice();
                    SendInvoice();
                    EndGarp();
                    RemoveOldLogFiles();
                }
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar" + Environment.NewLine);
            }
            catch (Exception e)
            {
                Console.Write("Error " + e);
                WriteToLog("Fel " + e.Message);

            }
        }

        public static bool ReadSettingsFile()
        {
            try
            {
                WriteToLog("Hämtar dokument från " + login.ReportInfo);

                ReportToRun = new List<ReportInfo>();
                string textlineReport, orgtextline;

                int countReports = 0;

                // Totalt antal rader i filen
                var lineCount = File.ReadAllLines(login.ReportInfo).Length;

                // Läs alla rader
                for (int i = 0; i < lineCount; i++)
                {
                    // Spara raden i orginal
                    orgtextline = File.ReadLines(login.ReportInfo).ElementAt(i);

                    // Rensa mellanslag
                    textlineReport = File.ReadLines(login.ReportInfo).ElementAt(i).Replace(" ", string.Empty);
                    if (textlineReport.Length >= 2)
                    {
                        ReportInfo reports = new ReportInfo();
                        countReports++;
                        string[] values = textlineReport.Split(',');
                        int countLineInfo = textlineReport.Split(',').Count();
                        string[] ReportValues = values[0].Split('-');
                        if (ReportValues[0].Length > 2)
                        {
                            reports.Generator = ReportValues[0];
                            reports.Report = ReportValues[1];
                        }
                        if (values[1] != "")
                        {
                            reports.Medium = values[1];
                        }
                        if (values[2] != "")
                        {
                            reports.Blankett = values[2];
                        }
                        if (countLineInfo > 3)
                        {
                            reports.Dialog1 = values[3];
                            reports.Answer1 = values[4];
                        }
                        ReportToRun.Add(reports);
                    }
                }
                WriteToLog(" - Klar" + Environment.NewLine);
                return true;
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid läsning av Rapportinfo-filen" + Environment.NewLine + e);
                return false;
            }
        }

        public static bool RunReportAndMail()
        {
            try
            {
                string FirstFSnumber = "0";
                string FirstDate = DateTime.Now.AddDays(-365).ToString("yyMMdd");
                bool Epost = false;
                bool EDIDirekt = false;
                bool EDIBuy = false;
                bool print = false;
                bool KAfind = false;
                bool KBfind = false;

                Garp.IReport rap;
                Garp.ITable tblHKA, tblTA01, tblKA, tblKB;
                tblHKA = GarpApp.Tables.Item("HKA");
                tblTA01 = GarpApp.Tables.Item("TA01");
                tblKA = GarpApp.Tables.Item("KA");
                tblKB = GarpApp.Tables.Item("KB");

                WriteToLog("Söker följesedlar att fakturera, fr.o.m. " + FirstDate + Environment.NewLine);

                // Hämtar första följesedel att fakturera, Fa-flagg = 1 eller 6, max 1 år gamla
                Garp.ITabFilter FSfilter = tblHKA.Filter;
                FSfilter.AddConst("0"); // 1
                FSfilter.AddConst("1"); // 2
                FSfilter.AddConst("9"); // 3
                FSfilter.AddConst(FirstDate); // 4
                FSfilter.AddField("OTY"); // 5
                FSfilter.AddField("FNR"); // 6
                FSfilter.AddField("LDT"); // 7
                FSfilter.AddField("FAF"); // 8
                FSfilter.AddConst("6"); // 9
                FSfilter.Expression = "5e3&7>4&((8=2&6<1)/8=9)"; // Ordertyp = 1-8, Fakturanr < 0, Senaste Leveranstid > gammalt datum, Fakturaflagga = 1 eller 6
                FSfilter.Active = true;

                tblHKA.First();
                FirstFSnumber = tblHKA.Fields.Item("HNR").Value;

                // Läs igenom följesedlarna och spara dokument som kommer att faktureras
                List<string> dokuments = new List<string>();

                WriteToLog("Lägger till dokument för utskrift ");

                while (tblHKA.Eof != true)
                {
                    KAfind = false;
                    KBfind = false;
                    tblTA01.Find(tblHKA.Fields.Item("BVK").Value); // Hämta betalningsvillkoret för aktuell följesedel

                    if (!dokuments.Any(x => x == tblTA01.Fields.Item("FAD").Value)) // Test om aktuellt dokument redan är inlagt för utskrift
                    {
                        WriteToLog(tblTA01.Fields.Item("FAD").Value + " ");
                        dokuments.Add(tblTA01.Fields.Item("FAD").Value); // lägg till dokumentet för utskrift
                    }

                    if (tblKA.Find(tblHKA.Fields.Item("FKN").Value)) // Hämta kundnummer från följesedeln
                    { KAfind = true; }

                    if (tblKB.Find(tblHKA.Fields.Item("FKN").Value)) // EDI-info finns
                    { KBfind = true; }

                    if (!Epost && KAfind
                        && tblKA.Fields.Item(login.KundKodEpost).Value != null) // Test om fakturan skall skickas via E-post
                    {
                        Epost = true;
                    }

                    if (!EDIDirekt && KBfind && tblTA01.Fields.Item("FAD").Value != null
                        && Tools.Left(tblTA01.Fields.Item("FAD").Value, 1) == login.Dok1tknDirekt // Test om Betalningsvillkorets dokument = inställning dok för direktfakturering
                        && tblKB.Fields.Item("EAM").Value != null)                                // EDI är inställt på kunden
                    {
                        EDIDirekt = true;
                        if (!EDIBuy)
                        {
                            WriteToLog(Environment.NewLine + "-- DET FINNS FAKTUROR SOM SKALL SKICKAS VIA EDI DIREKT --" + Environment.NewLine);
                        }
                        else
                        {
                            WriteToLog("Även EDI Dirket!" + Environment.NewLine);
                        }
                    }

                    if (!EDIBuy && KBfind && tblTA01.Fields.Item("FAD").Value != null
                        && Tools.Left(tblTA01.Fields.Item("FAD").Value, 1) == login.Dok1tknBuy // Test om Betalningsvillkorets dokument = inställning dok för fakturaköp
                        && tblKB.Fields.Item("EAM").Value != null)                             // EDI är inställt på kunden
                    {
                        EDIBuy = true;
                        if (!dokuments.Any(x => x == login.DokEDIKopia)) // Test om aktuellt dokument redan är inlagt för utskrift
                        {
                            dokuments.Add(login.DokEDIKopia);
                        }
                        if (!EDIDirekt)
                        {
                            WriteToLog(Environment.NewLine + "-- DET FINNS FAKTUROR SOM SKALL SKICKAS VIA EDI --" + Environment.NewLine);
                        }
                        else
                        {
                            WriteToLog("Även EDI och Köp" + Environment.NewLine);
                        }
                    }

                    if (
                        !print && KAfind && tblTA01.Fields.Item("FAD").Value != null
                        && tblKA.Fields.Item(login.KundKodEpost).Value == null                    // Test att fakturan INTE skall skickas via E-post
                        && Tools.Left(tblTA01.Fields.Item("FAD").Value, 1) == login.Dok1tknDirekt // Test om Betalningsvillkorets dokument = inställning dok för direktfakturering
                        && (!KBfind || (KBfind && tblKB.Fields.Item("EAM").Value == null))        // EDI är INTE inställt på kunden
                        )
                    {
                        print = true;
                        WriteToLog(Environment.NewLine + "Mottagare finns för utskrift till skrivare" + Environment.NewLine);
                    }

                    tblHKA.Next();
                }
                FSfilter.Active = false;

                WriteToLog(Environment.NewLine);

                if (FirstFSnumber != null)
                {
                    WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar utskrift av rapporter från följesedel " + FirstFSnumber + Environment.NewLine);
                    foreach (var report in ReportToRun)
                    {
                        rap = GarpApp.ReportGenerators.Item(report.Generator).Reports.Item(report.Report);
                        if (rap == null)
                        {
                            WriteToLog("HITTAR INTE FAKTURADOKUMENT " + report.Generator + "-" + report.Report + Environment.NewLine);
                        }
                        else
                        {
                            // Skriv bara ut dokumentet om det fanns följesedlar att fakturera med det dokumentet
                            if (dokuments.Any(x => x == report.Report))
                            {
                                bool printDok = false;
                                if (report.Medium != "P") // Om medium INTE är P skall dokumentet skrivas
                                {
                                    printDok = true;
                                }
                                else if (Tools.Left(report.Report, 1) == login.Dok1tknDirekt && print) // && !EDIDirekt) // Om medium är P, direkt och villkor för utskrift är uppfyllt. Tagit bort && !EDIDirekt) /Per N 2020-11-18
                                {
                                    printDok = true;
                                }
                                else if (Tools.Left(report.Report, 1) == login.Dok1tknBuy) // Om medium är P och fakturaköp skall dokument skrivs ut
                                {
                                    printDok = true;
                                }

                                if (report.Answer1 == "J" && (EDIDirekt || EDIBuy)) // Aktuellt dokument är för EDI-direkt eller EDI-köp
                                {
                                    Console.Write(Environment.NewLine + "-- BYT AVTAL TILL EDI --" + Environment.NewLine + Environment.NewLine + "Tryck sedan Enter");
                                    string answer = Console.ReadLine();
                                    WriteToLog("Avtal bytt till EDI:" + answer + "!" + Environment.NewLine);
                                    printDok = true;
                                }

                                if (report.Answer1 == "J" && !EDIDirekt && !EDIBuy) // Om det inte fanns EDI fakturor som går direkt skall ingen utskrift ske
                                {
                                    printDok = false;
                                }

                                if (report.Answer1 == "J" && !EDIDirekt && Tools.Left(report.Report, 1) == login.Dok1tknDirekt) // EDI skall skickas men inte direktfaktura
                                {
                                    printDok = false;
                                }

                                if (report.Answer1 == "J" && !EDIBuy && Tools.Left(report.Report, 1) == login.Dok1tknBuy) // EDI skall skickas men inte fakturaköp
                                {
                                    printDok = false;
                                }

                                if (printDok)
                                {

                                    WriteToLog("Skriver ut " + report.Generator + "-" + report.Report);

                                    switch (report.Medium)
                                    {
                                        case "D":
                                            if (login.FolderArchive != null && login.FolderArchive != "")
                                            {
                                                WriteToLog(" till " + login.FolderArchive);
                                                rap.FilePath = login.FolderArchive;
                                            }
                                            else
                                            {
                                                WriteToLog(" sökväg enligt blankett i Garp");
                                            }
                                            break;
                                        case "0":
                                            WriteToLog(" - ingen utskrift");
                                            break;

                                        case "P":
                                            WriteToLog(" till skrivare");
                                            break;

                                        default:
                                            WriteToLog(" inget eller fel medium angett");
                                            break;
                                    }


                                    rap.RangeFrom = FirstFSnumber;
                                    rap.RangeTo = "999999";

                                    if (report.Blankett != null)
                                    {
                                        rap.Form = report.Blankett;
                                        WriteToLog(", " + rap.Form);
                                    }
                                    if (report.Medium != null)
                                    {
                                        rap.Medium = report.Medium;
                                        WriteToLog(", " + rap.Medium);
                                    }
                                    if (report.Dialog1 != null)
                                    {
                                        rap.SetDialogResponse(report.Dialog1, report.Answer1);
                                        WriteToLog(", " + report.Dialog1 + " " + report.Answer1);
                                    }

                                    rap.Run();
                                    rap.Wait();
                                    WriteToLog(Environment.NewLine);
                                }
                                else
                                {
                                    WriteToLog("Ingen faktura för dokument " + report.Report + ", " + report.Medium + ", " + report.Blankett + " " + report.Dialog1 + Environment.NewLine);
                                }
                            }
                            else
                            {
                                WriteToLog("Ingen faktura för dokument " + report.Report + Environment.NewLine);
                            }
                        }
                    }
                }
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar med utskrifter" + Environment.NewLine);
                tblHKA = null;
                tblTA01 = null;
                tblKA = null;
                tblKB = null;
                FSfilter = null;
                rap = null;
                return true;
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid utskrift av rapporter" + Environment.NewLine + e + Environment.NewLine);
                return false;
            }
        }

        private static bool SendInvoice()
        {
            // Starta program för arkivering och utskick E-post
            try
            {
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar arkivering av fakturorna ");
                var proc = Process.Start(login.AppArchive, login.AppStartArgument);
                proc.WaitForExit(1800000); // Vänta max 30 minuter
                WriteToLog("- Klar" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog(Environment.NewLine + "Fel vid arkivering av fakturor" + Environment.NewLine + e + Environment.NewLine);
                return false;
            }

            try
            {
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar utskick fakturor via mail ");
                var proc = Process.Start(login.AppMail, login.AppStartArgument);
                proc.WaitForExit(1800000);
                WriteToLog("- Klar" + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog(Environment.NewLine + "Fel vid skicka fakturor via mail" + Environment.NewLine + e + Environment.NewLine);
                return false;
            }

            return true;

        }

        private static bool SendAribaInvoice()
        {
            bool result = false;

            try
            {
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar SendAribaInvoice" + Environment.NewLine);
                result = Rest.GetData(login.UrlAribaApi,"invoice:Mafi2023");
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + "Klar, med resultat: " + result.ToString() + Environment.NewLine);
            }
            catch (Exception e)
            {
                WriteToLog(Environment.NewLine + "Fel vid skicka fakturor via Ariba" + Environment.NewLine + e + Environment.NewLine);
            }

            return result;
        }

        private static bool ReadyToStart()
        {
            try
            {
                Console.Write(Environment.NewLine + "ÄR AVTALET RÄTT INSTÄLLT?");
                string answer = Console.ReadLine();
                if (answer != null && Tools.Left(answer, 1).ToUpper() == "J")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                if (login.SkrivLoggFil != null)
                {
                    WriteToLog("Fel uppstod vid uppstart " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
                }
                return false;
            }
        }


        private static void GarpLogin()
        {
            try
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GarpApp = new Garp.Application();

                //Loggar in i Garp och sätter bolag, användare och lösen
                WriteToLog("Loggar in i Garp. ");
                if (login.GarpConfig != null && login.GarpConfig != "")
                {
                    GarpApp.ChangeConfig(login.GarpConfig);
                }

                if (login.TraningMode == true)
                {
                    GarpApp.SwitchToTrainingMode();

                    if (GarpApp.IsTrainingMode != true)
                    {
                        GarpApp = null;
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        WriteToLog("Kan ej logga in i Övningsbolaget!" + Environment.NewLine);

                        Environment.Exit(0);
                    }
                    else
                    {
                        WriteToLog("ÖVNINGSBOLAGET! ");
                    }
                }

                GarpApp.Login(login.Usr, login.Password);
                GarpApp.SetBolag(login.Bolag);

                if (GarpApp.User == null)
                {
                    GarpApp = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine);

                    Environment.Exit(0);
                }

                WriteToLog("Inloggad som " + GarpApp.User + ", terminal " + GarpApp.Terminal + " och bolag " + GarpApp.Bolag + Environment.NewLine);
            }
            catch (Exception e)
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine + e + Environment.NewLine + e.Message);

                Environment.Exit(0);
            }
        }

        private static void EndGarp()
        {
            //Stäng Garp
            GarpApp = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            WriteToLog("Stänger Garp" + Environment.NewLine);
        }

        public static void WriteToLog(string text)
        {
            try
            {
                Console.Write(text);
                if (login.SkrivLoggFil != null)
                {
                    File.AppendAllText(login.LogFileName, text);
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                if (login.SkrivLoggFil != null)
                {
                    WriteToLog("Fel uppstod vid skrivning logfil " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
                }
            }
        }
        public static void RemoveOldLogFiles()
        {
            try
            {
                WriteToLog("Raderar logfiler äldre än " + login.MonthSaveLog + " månader" + Environment.NewLine);
                string[] OldFilses = Directory.GetFiles(Path.GetDirectoryName(login.LogFileName));
                foreach (string file in OldFilses)
                {
                    FileInfo fileToDelete = new FileInfo(file);
                    if (fileToDelete.CreationTime < DateTime.Now.AddMonths(0 - login.MonthSaveLog))
                        fileToDelete.Delete();
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel uppstod vid radering gamla logfiler " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }
    }
}
